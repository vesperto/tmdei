# This makefile assumes latexmk location is in the PATH
# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: main.pdf all clean

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: main.pdf

# CUSTOM BUILD RULES

# In case you didn't know, '$@' is a variable holding the name of the target,
# and '$<' is a variable holding the (first) dependency of a rule.
# "raw2tex" and "dat2tex" are just placeholders for whatever custom steps
# you might have.

#%.tex: %.raw
#	./raw2tex $< > $@

#%.tex: %.dat
#	./dat2tex $< > $@

# MAIN LATEXMK RULE

# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.

# -interaction=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.

main.pdf: main.tex
	latexmk -r latexmk.rc -outdir=build -auxdir=build  -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make main.tex
	@-mv ./build/main.pdf ./ 2>/dev/null  
	aspell --conf ./aspell.conf -c ./ch1/chapter1.tex
	aspell --conf ./aspell.conf -c ./ch2/chapter2.tex
	aspell --conf ./aspell.conf -c ./ch3/chapter3.tex
	aspell --conf ./aspell.conf -c ./frontmatter/glossary.tex
	aspell --conf ./aspell.conf -c ./frontmatter/frontmatter.tex
	aspell --conf ./aspell.conf -c ./appendices/appendixA.tex
	aspell --conf ./aspell.conf -c ./main.tex

clean:
	latexmk -r latexmk.rc -outdir=build -auxdir=build -C

clean-all:
	@-rm -Rf `biber --cache` # might return error if files do not exist; this will not work in windows...
	@-rm -rf ./build 2>/dev/null # might return error if files do not exist; this will not work in windows...
